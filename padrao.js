// CÓDIGO EM ES5

// function X(result) {
//     var products = result.data.recommendation;
//     var product = result.data.reference;

//     var imgProduct = product['item'].imageName;
//     var nameProduct = product['item'].name;
//     var urlProduct = product['item'].detailUrl;
//     var priceProduct = product['item'].price;
//     var oldPriceProduct = product['item'].oldPrice;
//     var conditionsProduct = product['item']['productInfo'].paymentConditions;

//     document.getElementById("lista-produto-visitado").innerHTML = "<a href='"+urlProduct+"' target='_blank' title='"+nameProduct+"'> <div class='imagem-center'> <img src='"+imgProduct+"'></div> <p class='nome-produto'>"+nameProduct+"</p> <p class='preco-old-produto'> De: "+oldPriceProduct+"</p> <p class='preco-produto'> Por: "+priceProduct+"</p> <p class='forma-pagamento-produto'>"+conditionsProduct+"</p> </a> ";
    
//     var productRecommendation = "";

//     for (var i = 0; i < products.length; i++) {
    	
//         var imgProduct = products[i].imageName;
// 	    var nameProduct = products[i].name;
// 	    var urlProduct = products[i].detailUrl;
// 	    var priceProduct = products[i].price;
// 	    var oldPriceProduct = products[i].oldPrice;
// 	    var conditionsProduct = products[i]['productInfo'].paymentConditions;

// 	    productRecommendation += "<li><a href='"+urlProduct+"' target='_blank' title='"+nameProduct+"'> <div class='imagem-center'> <img src='"+imgProduct+"'></div> <p class='nome-produto'>"+nameProduct+"</p> <p class='preco-old-produto'> De: "+oldPriceProduct+"</p> <p class='preco-produto'>Por: "+priceProduct+"</p> <p class='forma-pagamento-produto'>"+conditionsProduct+"</p> </a> </li> ";
        
//     }

//     document.getElementById("lista-produtos-recomendados").innerHTML = productRecommendation;

// }

// CÓDIGO EM ES6
function X(result) {
        const recommendation = result.data.recommendation; // AQUI GUARDA OS DADOS DOS PRODUTOS RECOMENDADOS
        const reference = result.data.reference; // AQUI GUARDA OS DADOS DO PRODUTO REFERENCIA
        const productReference = reference.item;

        const $produtoVisitado = document.getElementById("lista-produto-visitado");
        const $produtosRecomendados = document.getElementById("lista-produtos-recomendados");

        $produtoVisitado.innerHTML = `<a href='${productReference.detailUrl.replace(/(\/\/)/g, 'http://')}' target='_blank' title='${productReference.name}'>
                                        <div class='imagem-center'>
                                        <img src='${productReference.imageName.replace(/(\/\/)/g, 'http://') }'>
                                        </div>
                                        <p class='nome-produto'>${productReference.name}</p>
                                        <p class='preco-old-produto'>${productReference.oldPrice ? productReference.oldPrice : ''}</p>
                                        <p class='preco-produto'>${productReference.price}</p>
                                        <p class='forma-pagamento-produto'>${productReference.productInfo.paymentConditions}</p>
                                      </a>`;

        $produtosRecomendados.innerHTML = recommendationTemplate(recommendation);
      }

      function recommendationTemplate(products) { // MONTO UM TEMPLATE PARA SER USADO NO ESTILO DOS PRODUTOS
        return products.map(product => `<li>
                                          <a href='${product.detailUrl.replace(/(\/\/)/g, 'http://')}' target='_blank' title='${product.name}'>
                                            <div class='imagem-center'>
                                            <img src='${product.imageName.replace(/(\/\/)/g, 'http://')}'>
                                            </div>
                                            <p class='nome-produto'>${product.name}</p>
                                            <p class='preco-old-produto'>${product.oldPrice ? product.oldPrice : ''}</p>
                                            <p class='preco-produto'>${product.price}</p>
                                            <p class='forma-pagamento-produto'>${product.productInfo.paymentConditions}</p>
                                          </a>
                                        </li>`).join('');
      }



    